//
//  Analytics.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 23/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class DefaultAnalytics: Analytics {
    let ga = GoogleAnalytics()

    func trackNetworkRequest(URLString: String) {
        let name = "NetworkRequest";
        let params = ["address": URLString,
                      "eventTime": Date()] as [String : Any];
        gaTrack(name: name, data: params)
    }
    
    private func gaTrack(name: String, data: [String: Any]?) {
        ga.trackEvent(withName: name, data: data)
    }
}

