//
//  UserDefaultDataProvider.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 23/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class UserDefaultDataProvider: UserDataProvider {
    private let APIURLString: String
    
    init(URLStringConstructor: APIURLStringConstructor) {
        APIURLString = URLStringConstructor.APIURLString(for: .selfUser)
    }
    
    let api:APIManager = DefaultAPIManager()
    
    func getSelfUser(_ completion: @escaping (User?) -> Void) {
        api.load(APIURLString, { (result, error) in
            let serializer = ResponseSerializer<User>()
            completion(serializer.serializeItem(data: result!, rootKeyPath: "").0)
        })
    }


}
