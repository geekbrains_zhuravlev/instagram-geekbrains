//
//  TagDefaultDataProvider.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class TagDefaultDataProvider: TagDataProvider {
    let URLConstructor: APIURLStringConstructor
    let APIManager: APIManager
    let responseSerializer: ResponseSerializer<Tag>
    init(URLConstructor: APIURLStringConstructor, APIManager: APIManager, responseSerializer: ResponseSerializer<Tag>) {
        self.URLConstructor = URLConstructor
        self.APIManager = APIManager
        self.responseSerializer = responseSerializer
    }
    func search(qeuery: String, _ completion: @escaping ([Tag]?) -> Void) {
        let URLString = URLConstructor.APIURLString(for: .tagSearch(query: qeuery))
        APIManager.load(URLString) {[weak self] data, error in
            guard let data = data else {
                completion(nil)
                return
            }
            
            completion(self?.responseSerializer.serializeList(data: data, rootKeyPath: "data").0)
        }
    }
    
}
