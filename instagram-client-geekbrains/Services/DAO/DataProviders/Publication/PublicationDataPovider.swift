//
//  InstagramPublicationDataPovider.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 23/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class PublicationDataProvider {
    let URLConstructor: APIURLStringConstructor
    let APIManager: APIManager
    let responseSerializer: ResponseSerializer<Publication>
    init(URLConstructor: APIURLStringConstructor, APIManager: APIManager, responseSerializer: ResponseSerializer<Publication>) {
        self.URLConstructor = URLConstructor
        self.APIManager = APIManager
        self.responseSerializer = responseSerializer
    }
    
    func getPublications(with tag: Tag, _ completion: @escaping ([Publication]?, Error?) -> Void) {
        let URLString = URLConstructor.APIURLString(for: .tagPublications(tagName: tag.name))
        getPublications(from: URLString, completion)
    }
    
    private func getPublications(from URLString: String, _ completion: @escaping ([Publication]?, Error?) -> Void) {
        APIManager.load(URLString) {[weak self] data, error in
            guard error == nil else {
                completion(nil, error!)
                return
            }
            
            let (publications, error) = self?.responseSerializer.serializeList(data: data!, rootKeyPath: "data") ?? (nil, nil)
            
            completion(publications, error)
        }
    }
    
    func getSelfUserPublications(_ completion: @escaping ([Publication]?, Error?) -> Void) {
        let URLString = URLConstructor.APIURLString(for: .selfPublications)
        getPublications(from: URLString, completion)
    }
}
