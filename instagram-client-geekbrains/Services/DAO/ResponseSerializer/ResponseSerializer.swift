//
//  ResponseSerializer.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 30/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class ResponseSerializer<Destination: Serializable> {
    func serializeList(data: Data, rootKeyPath: String) -> ([Destination]?, Error?) {
        
        guard let dictionary = try? JSONSerialization.jsonObject(with: data,
                                                                 options: .mutableContainers) as? [String: Any] else {
                                                                    return (nil, SerializationError())
        }
        
        guard let dictionaries = dictionary?["data"] as? [[String: Any]] else {
            return (nil, SerializationError())
        }
        
        let items = dictionaries.compactMap(Destination.init)
        
        return (items, nil)
    }
    
    func serializeItem(data: Data, rootKeyPath: String) -> (Destination?, Error?) {
        
        guard let dictionary = try? JSONSerialization.jsonObject(with: data,
                                                                 options: .mutableContainers) as? [String: Any] else {
                                                                    return (nil, SerializationError())
        }
        
        guard let dataDictionary = dictionary?["data"] as? [String: Any] else {
            return (nil, SerializationError())
        }
        
        let item = Destination.init(dictionary: dataDictionary)
        
        return (item, nil)
    }
}



class SerializationError: Error {
    var localizedDescription: String = "Ошибка обработки данных"
}

