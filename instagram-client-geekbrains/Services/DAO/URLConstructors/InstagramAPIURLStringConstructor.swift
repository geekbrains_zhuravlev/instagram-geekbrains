//
//  InstagramURLConstructor.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 23/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation



class InstagramAPIURLStringConstructor: APIURLStringConstructor  {

    private let host = "https://api.instagram.com/v1"
    private let tokenParamName = "access_token"
    
    private let token: String
    
    init(token: String) {
        self.token = token
    }
    
    
    func APIURLString(for endPoint:APIEndPoint) -> String{
        
        let middlePart: String
        switch endPoint {
        case .selfUser:
            middlePart = "/users/self?"
        case .selfPublications:
            middlePart = "/users/self/media/recent?"
        case .tagSearch(let query):
            middlePart = "/tags/search?q=\(query)&"
        case .tagPublications(let tagName):
            middlePart = "/tags/\(tagName)/media/recent?"
        }
        
        return host + middlePart + "\(tokenParamName)=\(token)"
    }
    
}
