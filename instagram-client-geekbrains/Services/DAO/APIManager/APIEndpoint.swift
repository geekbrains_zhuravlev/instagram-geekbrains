//
//  APIEndPoint.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 23/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

enum APIEndPoint {
    case selfUser
    case selfPublications
    case tagSearch(query: String)
    case tagPublications(tagName: String)
}
