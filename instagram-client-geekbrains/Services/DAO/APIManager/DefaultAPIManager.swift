//
//  APIManager.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 18/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class DefaultAPIManager: APIManager {
    func load(_ urlString: String, _ completion: @escaping (Data?, Error?) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(nil, DataReceiveError())
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, error == nil else {
                completion(nil,  DataReceiveError())
                return
            }
            completion(data, nil)
        }.resume()
    }
}
