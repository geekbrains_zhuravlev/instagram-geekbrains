//
//  NetworkError.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 07/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class DataReceiveError: Error {
    var localizedDescription = "Ошибка получения данных :("
}
