//
//  APIManager.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 23/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol APIManager {
    func load(_ urlString: String, _ completion: @escaping (Data?, Error?) -> Void)
}
