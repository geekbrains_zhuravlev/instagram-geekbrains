//
//  FirstViewControllerFactory.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 21/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

enum FirstViewControllerType {
    case authentication
    case selfUser
}

class FirstViewControllerFactory {
    
    
    func viewController(for type: FirstViewControllerType) -> UIViewController {
        switch type {
        case .selfUser:
            let factory = PublicationListViewControllerFactory()
            return UINavigationController(rootViewController: factory.selfUserViewController())
        case .authentication:
            let builder = AuthenticationDefaultBuilder()
            return builder.build()
        }
    }
    
    func viewController(isUserAuthenticated: Bool) -> UIViewController{
        let type: FirstViewControllerType = isUserAuthenticated ? .selfUser : .authentication
        return viewController(for: type)
    }
}
