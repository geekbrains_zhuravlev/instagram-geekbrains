//
//  AppDelegate.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 18/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow()
        setFirstController()
        self.window?.makeKeyAndVisible()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loggedIn), name: NSNotification.Name("LoggedIn"), object: nil)
        
        return true
    }
    
    @objc func loggedIn() {
        setFirstController()
    }
    
    func setFirstController () {
        let firstViewControllerFactory = FirstViewControllerFactory()
        self.window?.rootViewController = firstViewControllerFactory.viewController(isUserAuthenticated: Credential.isUserAuthenticated)
    }
}


