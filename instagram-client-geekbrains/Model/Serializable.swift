//
//  Serializeable.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 30/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol Serializable {
    init?(dictionary: [String: Any])
}
