//
//  Publication.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 23/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

//user
//location
//caption
//captionSender
//date
//likes

class Publication: Serializable {
    let identifier: String
    let user: User
    let imageWidth: Int
    let imageHeight: Int
    let imageURLString: String

    
    required init?(dictionary: [String:Any]) {
        guard let identifier = dictionary["id"] as? String,
            let user = User(dictionary: (dictionary["user"] as? [String : Any]) ?? [:]),
            let image = (dictionary["images"] as? [String:Any] ?? [:])["standard_resolution"] as? [String:Any],
            let imageWidth = image["width"] as? Int,
            let imageHeight = image["height"] as? Int,
            let imageURLString = image["url"] as? String
            else {
            return nil
        }
        
        
        self.identifier = identifier
        self.user = user
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
        self.imageURLString = imageURLString
    }
}
