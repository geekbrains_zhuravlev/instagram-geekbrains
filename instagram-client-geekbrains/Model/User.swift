//
//  User.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 18/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class User: Serializable {
    let identifier: String
    let userName: String?
    let avatarImageURLString: String?
    let fullName: String?
    
    required init?(dictionary: [String : Any]) {
        guard let identifier = dictionary["id"] as? String else {
            return nil
        }
        self.identifier = identifier
        userName = dictionary["username"] as? String
        avatarImageURLString = dictionary["profile_picture"] as? String
        fullName = dictionary["full_name"] as? String
    }
}
