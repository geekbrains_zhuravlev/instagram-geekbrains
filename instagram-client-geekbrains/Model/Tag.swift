//
//  Tag.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class Tag: Serializable {
    let name: String
    let mediaCount: Int
    
    required init?(dictionary: [String : Any]) {
        guard let name = dictionary["name"] as? String else {
            return nil
        }
        self.name = name
        self.mediaCount = dictionary["media_count"] as? Int ?? 0
    }
}



