//
//  SearchPresenter.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class SearchPresenter: SearchViewOutput {
    weak var view: SearchView?
    var dataProvider: TagDataProvider!
    var tagPublicationsFactory: PublicationListViewControllerFactory!
    
    var searchQuery: String?
    var searchResults: [Tag]?
    
    func didSelectItem(with identifier: String) {
        guard let item = searchResults?.first(where: { (tag) -> Bool in
            tag.name == identifier
        }) else {
            return
        }
        
        view?.moveTo(viewController: tagPublicationsFactory.viewController(for: item))
    }
    
    var searchTask: DispatchWorkItem?
    func didChangeQuery(to query: String?) {
        guard let query = query else {
            self.searchResults = nil
            self.view?.results = nil
            return
        }
        
        self.searchTask?.cancel()
        self.searchTask = DispatchWorkItem(block: { [weak self] in
            self?.dataProvider.search(qeuery: query) { [weak self] (tags) in
                self?.searchResults = tags
                self?.view?.results = tags?.compactMap({ [weak self] in
                    self?.cellModel(from: $0)
                })
            }
        })
        self.searchQuery = query
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: self.searchTask!)
    }
    
    func didEndSearchQueryEditing() {
        view?.query = searchQuery
    }
    
    func viewWillAppear() {
        view?.query = searchQuery
    }
    
    
    func cellModel(from tag:Tag) -> SearchResultCellModel {
        let query = searchQuery ?? ""
        let attributedName = NSMutableAttributedString(string: tag.name)
        let font = UIFont.systemFont(ofSize: 14.0)
        attributedName.addAttribute(.font, value: font, range: NSMakeRange(0, tag.name.count))
        let boldFont = UIFont.systemFont(ofSize: 14.0, weight: .black)
        if let range = tag.name.lowercased().range(of: query.lowercased()) {
            attributedName.addAttribute(.font, value: boldFont, range: NSRange(range, in: tag.name))
        }
        
        return SearchResultCellModel(title: attributedName, subtitle:"Публикаций: \(tag.mediaCount)" , identifier: tag.name)
    }
}

