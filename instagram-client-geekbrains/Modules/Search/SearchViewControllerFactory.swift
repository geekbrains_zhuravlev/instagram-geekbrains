//
//  SearchVeiewControllerFactory.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class SearchViewControllerFactory {
    func viewController() -> UIViewController {
        let dataProvider = TagDefaultDataProvider(URLConstructor: InstagramAPIURLStringConstructor(token: Credential.token!), APIManager: DefaultAPIManager(), responseSerializer: ResponseSerializer<Tag>())
        let vc = SearchViewController()
        
        let presenter = SearchPresenter()
        presenter.view = vc
        presenter.dataProvider = dataProvider
        presenter.tagPublicationsFactory = PublicationListViewControllerFactory()
        
        vc.output = presenter
        return vc
    }
}
