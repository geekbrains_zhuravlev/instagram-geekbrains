//
//  SearchCellModel.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class SearchResultCellModel {
    let title: NSAttributedString
    let subtitle: String
    
    let identifier: String
    
    init(title: NSAttributedString, subtitle: String, identifier: String) {
        self.identifier = identifier
        self.title = title
        self.subtitle = subtitle
    }
}
