//
//  SearchViewController.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController, UISearchResultsUpdating {
    
    var searchController = UISearchController(searchResultsController: nil)
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableViewStyle.plain)
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    var output: SearchViewOutput!
    
    
    var query: String? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.searchController.searchBar.text = self?.query
            }
        }
    }
    
    var results: [SearchResultCellModel]? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    override func loadView() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.sizeToFit()
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        self.navigationItem.titleView = searchController.searchBar
        self.view = tableView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        guard searchController.isActive else {
            return
        }
        output.didChangeQuery(to: searchController.searchBar.text)
    }
    
}

extension SearchViewController: SearchView {
    func set(query: String) {
        searchController.searchBar.text = query
    }
    
    func moveTo(viewController: UIViewController) {
        searchController.isActive = false
        navigationController?.pushViewController(viewController, animated: true)
    }

}

extension SearchViewController: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        output.didEndSearchQueryEditing()
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let item = self.results?[indexPath.row] else {
            assertionFailure()
            return UITableViewCell()
        }
        let serachCellReuseId = "searchCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: serachCellReuseId)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: serachCellReuseId)
            
        }
        cell!.textLabel?.attributedText = item.title
        cell!.detailTextLabel?.text = item.subtitle
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let item = results?[indexPath.row] else {
            return
        }
        output.didSelectItem(with: item.identifier)
    }
    
}

