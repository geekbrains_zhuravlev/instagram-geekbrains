//
//  SearchView.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

protocol SearchView: class {
    var query: String? { get set }
    var results: [SearchResultCellModel]? { get set }
    
    func moveTo(viewController: UIViewController)
}
