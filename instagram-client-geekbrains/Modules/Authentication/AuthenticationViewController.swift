//
//  AuthenticationViewController.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 18/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import UIKit
import WebKit

class AuthenticationViewController: UIViewController {

    
    var router: AuthenticationRouter!
    
    let webView: WKWebView = WKWebView(frame: .zero)
    
    private let request: URLRequest = {
        let clientId = "ae634a63af024a6e989ab2d57359e7aa"
        let redirectURI = "https://instagram.com"
        
        let url = URL(string: "https://api.instagram.com/oauth/authorize/?client_id=\(clientId)&redirect_uri=\(redirectURI)&response_type=token")
        return URLRequest(url: url!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15.0)
    }()
    
    override func loadView() {
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        removeCache()
        webView.load(request)
    }
    
    private func removeCache() {
        let dataStore = WKWebsiteDataStore.default()
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { (records) in
            for record in records {
                dataStore.removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
            }
        }
    }
    
}


extension AuthenticationViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        guard let urlString =  navigationAction.request.url?.absoluteString else {
            decisionHandler(.allow)
            return
        }
        
        guard urlString.range(of: "#access_token") != nil else {
            decisionHandler(.allow)
            return
        }
        let accessToken = urlString.components(separatedBy: "#access_token=").last!
        Credential.token = accessToken
        NotificationCenter.default.post(name: NSNotification.Name("LoggedIn"), object: nil, userInfo: nil)
        decisionHandler(.cancel)
    }
}
