//
//  AuthenticationRouterInput.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 21/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol AuthenticationRouter: class {
    func afterAuthRedirect()
}
