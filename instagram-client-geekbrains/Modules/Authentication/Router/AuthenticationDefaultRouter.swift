//
//  AuthenticationRouter.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 21/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class AuthenticationDefaultRouter: AuthenticationRouter {
    weak var viewController: UIViewController?
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func afterAuthRedirect() {
        let mainViewController = UserViewController()
        viewController?.present(mainViewController, animated: true, completion: nil)
    }
}
