//
//  AuthenticationDefaultBuilder.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 21/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class AuthenticationDefaultBuilder: AuthenticationBuilder {
    func build() -> UIViewController {
        let viewController = AuthenticationViewController()
        let router = AuthenticationDefaultRouter(viewController: viewController as UIViewController)
        viewController.router = router
        return viewController
    }
}


