//
//  ViewController.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 18/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import UIKit

class UserViewController: UIViewController, PublicationListHeaderView {
    
    
    let dataProvider: UserDataProvider = {
        let constructor = InstagramAPIURLStringConstructor(token: Credential.token!)
        return UserDefaultDataProvider(URLStringConstructor: constructor)
    }()
    
    override var preferredContentSize: CGSize {
        get {
            return CGSize(width: 200, height: 300)
        }
        
        set {
            
        }
    }
    
    var user: User! {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.userNameLabel.text = self?.user.userName
            }
        }
    }
    let userNameLabel = UILabel()
    let editButton = UIButton(type: .system)
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        editButton.setTitle("edit", for: .normal)
        view.addSubview(userNameLabel)
        view.addSubview(editButton)
    }
    
    private func layout() {
        editButton.translatesAutoresizingMaskIntoConstraints = false
        userNameLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraint(NSLayoutConstraint(item: userNameLabel,
                                              attribute: .centerX,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .centerX,
                                              multiplier: 1.0,
                                              constant: 0.0))
        
        view.addConstraint(NSLayoutConstraint(item: userNameLabel,
                                              attribute: .centerY,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .centerY,
                                              multiplier: 1.0,
                                              constant: 0.0))
        
        view.addConstraint(NSLayoutConstraint(item: editButton,
                                              attribute: .centerX,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .centerX,
                                              multiplier: 1.0,
                                              constant: 0.0))
        
        view.addConstraint(NSLayoutConstraint(item: editButton,
                                              attribute: .top   ,
                                              relatedBy: .equal,
                                              toItem: userNameLabel,
                                              attribute: .bottom,
                                              multiplier: 1.0,
                                              constant: 20.0))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layout()
        editButton.addTarget(self, action: #selector(didPressEdit), for: .touchUpInside)
        refresh()
    }
    
    
    @objc func didPressEdit() {
        self.navigationController?.pushViewController(UserEditViewController(user: self.user), animated: true)
    }

    func refresh() {
        userNameLabel.text = "loading..."
        dataProvider.getSelfUser { [weak self] (user) in
            guard let user = user else {
                DispatchQueue.main.async {
                    self?.userNameLabel.text = "error"
                }
                return
            }
            self?.user = user
        }
    }
}

