//
//  UserEditViewController.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 31/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class UserEditViewController: UIViewController {
    
    @objc(UserEditViewControllerSnapshot) private class UserEditViewControllerSnapshot: NSObject, NSCoding, Snapshot {
        let userName: String?
        let fullName: String?
        let userId: String
        let identifier: String?
        
        func encode(with aCoder: NSCoder) {
            aCoder.encode(userName, forKey: "userName")
            aCoder.encode(fullName, forKey: "fullName")
            aCoder.encode(userId, forKey: "userId")
        }
        
        required init?(coder aDecoder: NSCoder) {
            userName = aDecoder.decodeObject(forKey: "userName") as? String
            fullName = aDecoder.decodeObject(forKey: "fullName") as? String
            userId = aDecoder.decodeObject(forKey: "userId") as! String
            identifier = nil
        }
        
        init(userName: String?, fullName: String?, userId: String, identifier: String) {
            self.userName = userName
            self.fullName = fullName
            self.userId = userId
            self.identifier = identifier
        }
        
    }
    
    let snapshotIdentifier = "UserEdit"
    let user: User
    let snapshotManager = SnapshotManager()
    
    private func constructTextField() -> UITextField {
        let textField = UITextField()
        textField.layer.borderColor = UIColor.lightGray.cgColor
        textField.layer.borderWidth = 1
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }
    
    private lazy var nameTextField: UITextField = self.constructTextField()
    private lazy var fullnameTextField: UITextField = self.constructTextField()
    
    init(user: User) {
        self.user = user
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        view.addSubview(nameTextField)
        view.addSubview(fullnameTextField)
        self.view = view
    }
    
    func layout() {
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[nameTextField]-20-[fullnameTextField]",
                                                           options: NSLayoutFormatOptions(rawValue: 0),
                                                           metrics: nil,
                                                           views: ["nameTextField": nameTextField,
                                                                   "fullnameTextField": fullnameTextField]
        ))
        
        view.addConstraint(NSLayoutConstraint(item: nameTextField,
                                              attribute: .centerX,
                                              relatedBy: .equal,
                                              toItem: fullnameTextField,
                                              attribute: .centerX,
                                              multiplier: 1.0,
                                              constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: fullnameTextField,
                                              attribute: .centerX,
                                              relatedBy: .equal,
                                              toItem: view,
                                              attribute: .centerX,
                                              multiplier: 1.0,
                                              constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: fullnameTextField,
                                              attribute: .width,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 100))
        
        view.addConstraint(NSLayoutConstraint(item: nameTextField,
                                              attribute: .width,
                                              relatedBy: .equal,
                                              toItem: nil,
                                              attribute: .notAnAttribute,
                                              multiplier: 1.0,
                                              constant: 100))
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let snapshot = snapshotManager.snapshot(with: snapshotIdentifier)
        restoreState(from: snapshot)

        layout()
    }
    
    func snapshot() -> Snapshot {
        return UserEditViewControllerSnapshot(userName: nameTextField.text,
                                              fullName: fullnameTextField.text,
                                              userId:self.user.identifier,
                                              identifier:snapshotIdentifier)
    }
    
    func restoreState(from snapshot: Snapshot?) {
        guard let snapshot = snapshot as? UserEditViewControllerSnapshot,
            snapshot.userId == self.user.identifier else {
            return
        }
        nameTextField.text = snapshot.userName
        fullnameTextField.text = snapshot.fullName
    }
}

extension UserEditViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text ?? ""
        let range = Range(range, in: oldText)
        textField.text = oldText.replacingCharacters(in: range!, with: string)
        snapshotManager.save(snapshot: self.snapshot())
        return false
    }
}
