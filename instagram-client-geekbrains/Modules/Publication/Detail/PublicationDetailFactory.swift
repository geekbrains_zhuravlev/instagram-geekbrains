//
//  PublicationDetailFactory.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 06/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class PublicationDetailFactory {
    func viewController(for publication: Publication) -> UIViewController {
        let vc =  UIViewController()
        vc.view.backgroundColor = .red
        return vc
    }
}
