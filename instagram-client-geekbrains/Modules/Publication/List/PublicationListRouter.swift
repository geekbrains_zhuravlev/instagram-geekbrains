//
//  PublicationListRouter.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 06/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class PublicationListRouter {
    let viewController: UIViewController
    let publicationDetailFactory: PublicationDetailFactory = PublicationDetailFactory()
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func showDetail(of publication: Publication) {
        viewController.navigationController?.pushViewController(publicationDetailFactory.viewController(for: publication), animated: true)
    }
}
