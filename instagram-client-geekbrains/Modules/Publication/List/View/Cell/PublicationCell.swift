//
//  PublicationCell.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class PublicationCell: UITableViewCell {
    @IBOutlet weak var mainImageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
}
