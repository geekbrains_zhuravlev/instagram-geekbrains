//
//  PublicationListView.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 07/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol PublicationListView {
    
    var items: [PublicationCellModel]?  { get set } 
    
    func showLoader()
    func hideLoader()
    func show(error: Error?)
    
    func hidePaginationLoader()
    
}
