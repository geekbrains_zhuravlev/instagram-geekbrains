//
//  PublicationListViewController.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 21/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import PKHUD


class PublicationListViewController: UIViewController, PublicationListView {
    
    var output: PublicationListViewOutput!
    
    var headerViewController: (UIViewController & PublicationListHeaderView)!
    
    var items: [PublicationCellModel]? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.refreshControl?.endRefreshing()
                self?.tableView.reloadData()
            }
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let cellReuseId = "PublicationCell"
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableViewStyle.plain)
        tableView.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        
        let footerView = PaginationLoaderView(frame: CGRect(x: 0, y: 0, width: 0, height: 100))
        tableView.tableFooterView = footerView
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        
        let headerView = headerViewController.view
        headerView?.frame = CGRect(origin: .zero, size: headerViewController.preferredContentSize)
        tableView.tableHeaderView = headerView
        
        
        let nib = UINib(nibName: "PublicationCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellReuseId)
        return tableView
    }()
    
    override func loadView() {
        view = tableView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let headerViewController = headerViewController {
            self.addChildViewController(headerViewController)
        }
        
        output.viewReady()
    }
    
    func hideLoader() {
        DispatchQueue.main.async {
            HUD.hide()
        }
    }
    
    func showLoader() {
        DispatchQueue.main.async {
            HUD.show(.progress)
        }
    }
    
    func show(error: Error?) {
        DispatchQueue.main.async {
            HUD.show(.label(error?.localizedDescription))
        }
    }
    
    func hidePaginationLoader() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.tableFooterView = nil
        }
        
    }
    
    @objc func refresh() {
        headerViewController.refresh()
        output.refreshed()
    }
    
}

extension PublicationListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }

    func height(forItemAt index: Int) -> CGFloat{
        guard index < (items?.count ?? 0),
            let item = items?[index] else {
                return 0
        }
        let height =  self.view.bounds.width * CGFloat(item.imageHeight) / CGFloat(item.imageWidth) + 31
        return height
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return height(forItemAt: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return height(forItemAt: indexPath.row)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? PublicationCell,
            indexPath.row < (items?.count ?? 0),
            let item = items?[indexPath.row]
            else {
            return UITableViewCell()
        }
        if let url = URL(string:item.imageURLString ?? "") {
            cell.mainImageView?.kf.setImage(with: url)
        }
        
        cell.titleLabel?.text = item.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard indexPath.row < (items?.count ?? 0),
            let item = items?[indexPath.row] else {
                return 
        }
        output.didSelectItem(with: item.identifier)
    }

    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnded()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollEnded()
    }
    
    func scrollEnded() {
        let currentOffset = tableView.contentOffset.y
        let maximumOffset = tableView.contentSize.height - tableView.frame.size.height - 100
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            output.scrolledToBottom()
        }
    }
}



