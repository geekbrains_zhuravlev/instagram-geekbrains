//
//  PublicationListHeaderView.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 08/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol PublicationListHeaderView {
    func refresh()
}
