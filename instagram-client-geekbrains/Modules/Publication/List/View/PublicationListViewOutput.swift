//
//  PublicationListOutput.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol PublicationListViewOutput {
    func didSelectItem(with identifier: String)
    func scrolledToBottom()
    func refreshed()
    func viewReady()
}
