//
//  PublicationListFactory.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation
import UIKit

class PublicationListViewControllerFactory {
    func selfUserViewController() -> UIViewController {
        let dataProvider = SelfUserPublicationsProvider(URLConstructor: InstagramAPIURLStringConstructor(token: Credential.token!), APIManager: DefaultAPIManager(), responseSerializer: ResponseSerializer<Publication>())
        return viewController(with: dataProvider)
    }
    
    func viewController(for tag: Tag) -> UIViewController {
        let dataProvider = TagPublicationsProvider(tag: tag, URLConstructor: InstagramAPIURLStringConstructor(token: Credential.token!), APIManager: DefaultAPIManager(), responseSerializer: ResponseSerializer<Publication>())
        return viewController(with: dataProvider)
    }
    
    private func viewController(with dataProvider: PublicationListDataProvider) -> UIViewController{
        
        let view = PublicationListViewController()
        view.headerViewController = UserViewController()
        
        let presenter = PublicationListPresenter()
        presenter.view = view
        view.output = presenter
        
        let interactor = PublicationListDefaultInteractor()
        interactor.output = presenter
        interactor.dataProvider = dataProvider
        presenter.interactor = interactor
        
        let router = PublicationListRouter(viewController: view)
        
        presenter.router = router
        
        return view
    }
}
