//
//  TagPublicationsProvider.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 07/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class TagPublicationsProvider: PublicationListDataProvider {
    let baseProvider: PublicationDataProvider
    let tag: Tag
    
    init(tag: Tag, URLConstructor: APIURLStringConstructor, APIManager: APIManager, responseSerializer: ResponseSerializer<Publication>) {
        self.baseProvider = PublicationDataProvider(URLConstructor: URLConstructor, APIManager: APIManager, responseSerializer: responseSerializer)
        self.tag = tag
    }
    
    func getPublications(page: Int, _ completion: @escaping ([Publication]?, Error?) -> Void) {
        baseProvider.getPublications(with: tag, completion)
    }
    
    
}
