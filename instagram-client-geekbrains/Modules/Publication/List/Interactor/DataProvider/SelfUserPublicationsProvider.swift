//
//  SelfUserPublicationsProvider.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 07/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class SelfUserPublicationsProvider: PublicationListDataProvider {
    let baseProvider: PublicationDataProvider

    init(URLConstructor: APIURLStringConstructor, APIManager: APIManager, responseSerializer: ResponseSerializer<Publication>) {
        self.baseProvider = PublicationDataProvider(URLConstructor: URLConstructor, APIManager: APIManager, responseSerializer: responseSerializer)
    }
    
    func getPublications(page: Int, _ completion: @escaping ([Publication]?, Error?) -> Void) {
        guard page < 4 else {
            completion([], nil)
            return
        }
        baseProvider.getSelfUserPublications(completion)
    }
    
    
}
