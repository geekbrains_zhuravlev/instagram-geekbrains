//
//  PublicationListInteractor.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 07/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol PublicationListInteractor: class {
    func getPublications()
    func reset()
    func publication(with identifier: String) -> Publication?
}
