//
//  PublicationListSelfUserInteractor.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 07/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

//TODO пагинация

class PublicationListDefaultInteractor: PublicationListInteractor {
    
    weak var output: PublicationListInteractorOutput!
    var dataProvider: PublicationListDataProvider!
    var page = 0
    
    var publications: [Publication] = []
    
    
    func getPublications() {
        self.dataProvider.getPublications(page: page) { [weak self] (publications, error) in
            guard error == nil else {
                self?.output.got(error: error!)
                return
            }
            self?.publications = (self?.publications ?? []) + (publications ?? [])
            self?.output.got(publications: publications ?? [], page: self?.page ?? 0)
            self?.page += 1
        }
    }
    
    func reset() {
        self.page = 0
    }
    
    func publication(with identifier: String) -> Publication? {
        return publications.first(where: {
            return $0.identifier == identifier
        })
    }
}
