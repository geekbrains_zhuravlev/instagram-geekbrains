//
//  PublicationCellModel.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 04/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class PublicationCellModel {
    let imageURLString: String?
    let imageWidth: Int
    let imageHeight: Int
    let title: String?
    let identifier: String
    
    init(identifier: String, title: String?, imageURLString: String?, imageWidth: Int, imageHeight: Int) {
        self.identifier = identifier
        self.imageURLString = imageURLString
        self.title = title
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight
    }
}
