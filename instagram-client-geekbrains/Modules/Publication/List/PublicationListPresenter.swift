//
//  PublicationListPresenter.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 07/09/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class PublicationListPresenter  {
    var interactor: PublicationListInteractor!
    var view: PublicationListView!
    var router: PublicationListRouter!
    var isLoading = false
    var lastPageLoaded = false
    
    func cellModel(from publication: Publication) -> PublicationCellModel {
        return PublicationCellModel(identifier: publication.identifier, title: publication.user.userName, imageURLString: publication.imageURLString, imageWidth: publication.imageWidth, imageHeight: publication.imageHeight)
    }
    
    func getPublications() {
        guard isLoading == false, lastPageLoaded == false else {
            return
        }
        isLoading = true
        interactor.getPublications()
    }
    
}

extension PublicationListPresenter: PublicationListViewOutput {
    func scrolledToBottom() {
        getPublications()
    }
    
    func refreshed() {
        interactor.reset()
        lastPageLoaded = false
        getPublications()
        
    }
    
    func didSelectItem(with identifier: String) {
        guard let publication = interactor.publication(with: identifier) else {
            return
        }
        router.showDetail(of: publication)
    }
    
    func viewReady() {
        getPublications()
    }
    
    
}

extension PublicationListPresenter: PublicationListInteractorOutput {
    
    func got(publications: [Publication], page: Int) {
        guard publications.count > 0 else {
            lastPageLoaded = true
            view.hidePaginationLoader()
            return
        }
        
        let items = publications.compactMap({ [weak self] in
            return self?.cellModel(from: $0)
        })
        let oldItems = page == 0 ? [] : view.items ?? []
        
        let newItems = oldItems + items
        
        view.items = newItems
        isLoading = false
    }
    
    func got(error: Error) {
        isLoading = false
        view.show(error: error)
    }
}
