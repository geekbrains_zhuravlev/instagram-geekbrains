//
//  SnapshotManager.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 31/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

class SnapshotManager {
    let storage = UserDefaults.standard
    func save(snapshot: Snapshot) {
        guard snapshot.identifier != nil else {
            assertionFailure("can't save snapshot without identifier")
            return
        }
        storage.set(NSKeyedArchiver.archivedData(withRootObject: snapshot), forKey: storageKey(for: snapshot.identifier!))
        storage.synchronize()
    }
    
    func snapshot(with identifier: String) -> Snapshot?{
        
        guard let data = storage.object(forKey: storageKey(for: identifier)) as? Data else {
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? Snapshot
    }
    
    private func storageKey(for identifier: String) -> String {
        return "snapshot" + identifier
    }
}
