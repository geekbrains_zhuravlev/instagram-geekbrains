//
//  Snapshot.swift
//  instagram-client-geekbrains
//
//  Created by Aleksandr Zhuravlev on 31/08/2018.
//  Copyright © 2018 Aleksandr Zhuravlev. All rights reserved.
//

import Foundation

protocol Snapshot {
    var identifier: String? { get }
}
